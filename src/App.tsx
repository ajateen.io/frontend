import DateFnsUtils from '@date-io/date-fns';
import { green, lightGreen } from '@material-ui/core/colors';
import { CssBaseline } from '@material-ui/core';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import * as React from 'react';
import * as Reflux from 'reflux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import { MainPage } from './pages/MainPage';
import { LoginPage } from './pages/LoginPage';

const theme = createMuiTheme({
  palette: {
    background: {
      default: lightGreen[200],
      paper: lightGreen.A100,
    },
    primary: {
      main: lightGreen[900],
    },
    secondary: {
      main: green[500],
    },
    type: 'light',
  },
});

export class App extends Reflux.Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <CssBaseline />
          <Router>
            <Route exact={true} path='/' component={MainPage} />
            <Route path='/login' component={LoginPage} />
          </Router>
        </MuiPickersUtilsProvider>
      </MuiThemeProvider>
    );
  }
}
