import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import { DateTimePicker } from "@material-ui/pickers";
import * as React from 'react';
import * as Reflux from 'reflux';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { DataStore, closeNewGroupDialog } from 'src/State';

export class NewGroupDialog extends Reflux.Component {
  state = {
    isAddingNewGroup: false,
    activity: "",
    startTime: undefined,
    isStartTimeOpen: false,
    endTime: undefined,
    isEndTimeOpen: false,
    location: "",
    maxParticipants: 0,
  };

  constructor(props : {}) {
    super(props);
    this.store = DataStore;
    this.storeKeys = ['isAddingNewGroup'];
  }

  render() {
    return (
      <Dialog
        open={Boolean(this.state.isAddingNewGroup)}
        onClose={closeNewGroupDialog}
        aria-labelledby='new-group'
      >
        <DialogTitle id='new-group'>New Group For Activity</DialogTitle>

        <DialogContent>
          <DialogContentText>
            Fill in information to add new group:
          </DialogContentText>

          <TextField
            margin='none'
            label='Activity'
            type='text'
            fullWidth={true}
            required={true}
            onInput={this.handleActivityChange}
            value={this.state.activity}
          />

          <DateTimePicker
            onClick={this.openStartTime}
            onClose={this.closeStartTime}
            label="Start Date/Time"
            value={this.state.startTime}
            onChange={this.handleStartTimeChange}
            ampm={false}
            disablePast={true}
            open={this.state.isStartTimeOpen}
          />

          <DateTimePicker
            onClick={this.openEndTime}
            onClose={this.closeEndTime}
            label="End Date/Time"
            value={this.state.endTime}
            onChange={this.handleEndTimeChange}
            ampm={false}
            disablePast={true}
            open={this.state.isEndTimeOpen}
          />

          <TextField
            margin='none'
            label='Location'
            type='text'
            fullWidth={true}
            required={true}
            onInput={this.handleLocationChange}
            value={this.state.location}
          />

          <TextField
            margin='none'
            label='Max. participants'
            type='number'
            fullWidth={true}
            required={true}
            onInput={this.handleMaxParticipantsChange}
            value={this.state.maxParticipants}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={closeNewGroupDialog} color='secondary'>
            Cancel
          </Button>

          <Button onClick={this.addNewGroup} color='primary'>
            Add Group
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  private addNewGroup = () => {
    closeNewGroupDialog();
  }

  private handleActivityChange = (event : React.FormEvent<HTMLDivElement>) => {
    const activity = (event.target as TextFieldProps).value as string;
    this.setState({activity});
  }

  private openStartTime = () => this.setState({isStartTimeOpen : true});
  private openEndTime = () => this.setState({isEndTimeOpen : true});
  private closeStartTime = () => this.setState({isStartTimeOpen : false});
  private closeEndTime = () => this.setState({isEndTimeOpen : false});

  private handleStartTimeChange = (startTime: MaterialUiPickersDate) => {
    this.setState({startTime});
  }

  private handleEndTimeChange = (endTime: MaterialUiPickersDate) => {
    this.setState({endTime});
  }

  private handleLocationChange = (event : React.FormEvent<HTMLDivElement>) => {
    const location = (event.target as TextFieldProps).value as string;
    this.setState({location});
  }

  private handleMaxParticipantsChange = (event : React.FormEvent<HTMLDivElement>) => {
    const maxParticipants = (event.target as TextFieldProps).value as string;
    this.setState({maxParticipants : Number(maxParticipants)});
  }
}
