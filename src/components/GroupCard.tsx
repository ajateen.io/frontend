import Card from '@material-ui/core/Card';
import * as React from 'react';
import * as Reflux from 'reflux';
import './GroupCard.css';
import Typography from '@material-ui/core/Typography';

export class GroupCard extends Reflux.Component {
  render() {
    return (
      <Card className="group_card">
        <Typography variant="h5" className="group_participants">{this.props.group.participants}/{this.props.group.max_participants}</Typography>
        <Typography variant="h5" className="group_time">{this.formatNumber(this.props.group.start_time.getHours())}:{this.formatNumber(this.props.group.start_time.getMinutes())}</Typography>
      </Card>
    );
  }

  private formatNumber(value: number) : string {
    if (value < 10) {
      return `0${value}`;
    }

    return `${value}`;
  }
}
