import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import * as React from 'react';
import * as Reflux from 'reflux';
import './Bar.css';

export class Bar extends Reflux.Component {
  render() {
    return (
      <AppBar position="static" color="default" className="bar">
        <Toolbar>
          <Typography variant="h6" color="inherit">
            AJATEEN.IO
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}
