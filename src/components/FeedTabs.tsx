import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import * as React from 'react';
import * as Reflux from 'reflux';
import './FeedTabs.css';
import { DataStore, openCategoriesMenu, unsetCurrentCategory } from 'src/State';

interface Category {
  id: number;
  name_en: string;
  created_at: string | Date;
  updated_at: string | Date;
}

interface FeedTabsState {
  currentCategory?: Category;
  value: number;
}

export class FeedTabs extends Reflux.Component {
  state: FeedTabsState = {
    value: 0,
    currentCategory: undefined,
  };

  constructor(props : {}) {
    super(props);
    this.store = DataStore;
    this.storeKeys = ['currentCategory'];
  }

  render() {
    return (
      <Tabs
        className="feed_tabs"
        value={this.state.value}
        onChange={this.handleChange}
        indicatorColor="primary"
        textColor="primary"
        variant="fullWidth"
        centered={true}
      >
        <Tab label="All" />
        <Tab onClick={this.openCategoriesMenu} label={this.state.currentCategory ? this.state.currentCategory.name_en : "By Category"} />
      </Tabs>
    );
  }

  private handleChange = (event: React.ChangeEvent<{}>, value: number) => {
    unsetCurrentCategory();
    this.setState({ value });
  }


  private openCategoriesMenu = (event: React.MouseEvent<HTMLElement>) => {
    openCategoriesMenu(event.currentTarget as HTMLElement);
  }
}
