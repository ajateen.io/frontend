import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import IsHereIcon from '@material-ui/icons/HowToReg';
import MarkHereIcon from '@material-ui/icons/HowToRegOutlined';
import * as React from 'react';
import * as Reflux from 'reflux';
import './JoinedList.css';
import { DataStore, closeJoinedList } from 'src/State';

interface Activity {
  id: number;
  name_en: string;
  category_id: number;
  created_at: Date;
  updated_at: Date;
}

interface Soldier {
  id: number;
  first_name: string;
  last_name: string;
  identity_code: number;
  rank_id: number;
  superior_id: Soldier;
  created_at: Date;
  updated_at: Date;
}

interface FullGroupData {
  id: number;
  start_time: Date;
  end_time: Date;
  max_participants: number;
  location: string;
  created_at: Date;
  updated_at: Date;
  activity: Activity;
  soldiers: Soldier[];
}

interface JoinedListState {
  groupDetails?: FullGroupData;
  isJoinedListOpen: boolean;
}

export class JoinedList extends Reflux.Component {
  state: JoinedListState = {
    groupDetails: undefined,
    isJoinedListOpen: false,
  };

  constructor(props : {}) {
    super(props);
    this.store = DataStore;
    this.storeKeys = ['groupDetails', 'isJoinedListOpen'];
  }

  render() {
    const group = this.state.groupDetails;

    return (
      <Dialog
        open={this.state.isJoinedListOpen}
        onClose={closeJoinedList}
      >
        <div className="group_details">
          <DialogTitle id='group_details'>Joined List</DialogTitle>
          <DialogContent>
            <DialogContentText>
              <ol>
                {group ? group.soldiers.map((soldier, index) => (
                    <li>{soldier.first_name} {soldier.last_name} <IconButton>{index % 2 === 0 ? <MarkHereIcon/> : <IsHereIcon/>}</IconButton></li>
                  )) : ""
                }
              </ol>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={closeJoinedList} color='secondary'>
              Close
            </Button>
          </DialogActions>
        </div>
      </Dialog>
    );
  }
}