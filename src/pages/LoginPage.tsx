import Button from '@material-ui/core/Button';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import * as React from 'react';
import * as Reflux from 'reflux';
import './LoginPage.css';
import { Typography } from '@material-ui/core';

interface LoginPageState {
  username: string;
  password: string;
  error: string;
}

export class LoginPage extends Reflux.Component {
  state: LoginPageState = {
    username: '',
    password: '',
    error: '',
  };

  render() {
    return (
      <div className='login_container'>
        <Typography variant="h4" color="primary">
          AJATEEN.IO
        </Typography>
        <TextField
          margin='normal'
          label='Username'
          fullWidth={true}
          required={true}
          onInput={this.handleUsernameChange}
          value={this.state.username}
        />
        <TextField
          error={Boolean(this.state.error)}
          helperText={this.state.error}
          margin='normal'
          label='Password'
          type="password"
          fullWidth={true}
          required={true}
          onInput={this.handlePasswordChange}
          value={this.state.password}
        />
        <Button color='primary' variant='outlined' fullWidth={true} onClick={this.login}>
          Login
        </Button>
      </div>
    );
  }

  componentDidMount() {
    // If user is logged in - redirect him
    if (localStorage.getItem('token')) {
      this.props.history.push('/');
    }
  }

  private handleUsernameChange = (event : React.FormEvent<HTMLDivElement>) => {
    const username = (event.target as TextFieldProps).value as string;
    this.setState({username, error : ''});
  }

  private handlePasswordChange = (event : React.FormEvent<HTMLDivElement>) => {
    const password = (event.target as TextFieldProps).value as string;
    this.setState({password, error : ''});
  }

  private login = () => {
    let fail = false;
    fetch(`/api/sessions?email=${encodeURIComponent(this.state.username)}&password=${encodeURIComponent(this.state.password)}`, {
      credentials : 'same-origin',
      method : 'POST',
    }).then(response => {
      fail = response.status !== 200;
      if (fail) {
        return null;
      }

      return response.json();
    }).then(responseJson => {
      if (!fail) {
        localStorage.setItem('email', responseJson.email);
        localStorage.setItem('token', responseJson.authentication_token);
        localStorage.setItem('soldierId', responseJson.soldier.id);
        // TODO: Save soldier object somewhere!
        window.location.href = "/";
        return;
      }

      this.setState({error : "Wrong credentials!"});
    });
  }
}